unit UVenda;

interface
uses
  UProduto, System.SysUtils, ZDataSet, UDM;

type

  TVenda = class

  private
    FId : Integer;
    FProduto : TProduto;
    FDataHora : TDateTime;
    FQuantidade : Integer;
    FValorUnitario : Double;
    FPercentualDesconto : Double;
    function getDataHora: TdateTime;
    function getId: Integer;
    function getPercentualDesconto: Double;
    function getProduto: TProduto;
    function getQuantidade: Integer;
    function getValorUnitario: Double;
    procedure setDataHora(const Value: TdateTime);
    procedure setId(const Value: Integer);
    procedure setPercentualDesconto(const Value: Double);
    procedure setProduto(const Value: TProduto);
    procedure setQuantidade(const Value: Integer);
    procedure setValorUnitario(const Value: Double);
  public
    property id : Integer read getId write setId;
    property produto : TProduto read getProduto write setProduto;
    property dataHora : TdateTime read getDataHora write setDataHora;
    property quantidade : Integer read getQuantidade write setQuantidade;
    property valorUnitario : Double read getValorUnitario write setValorUnitario;
    property PercentualDesconto : Double read getPercentualDesconto write setPercentualDesconto;

    procedure finalizarVenda(venda : TVenda);

  end;

implementation

{ TVenda }

procedure TVenda.finalizarVenda(venda: TVenda);
var
  qry : TZQuery;
  i   : Integer;
begin

  qry := TZQuery.Create(nil);
  qry.Connection := DM.conexao;
  qry.Close;

  qry.SQL.Clear;       
  qry.SQL.Add('insert into tbvenda');
  qry.SQL.Add('(');
  qry.SQL.Add('id_tbproduto,');
  qry.SQL.Add('data_hora,');
  qry.SQL.Add('quantidade,');
  qry.SQL.Add('valor_unitario,');        
  qry.SQL.Add('perc_desconto');        
  qry.SQL.Add(')');
  qry.SQL.Add('values');
  qry.SQL.Add('(');
  qry.SQL.Add(':id_tbproduto,');
  qry.SQL.Add(':data_hora,');
  qry.SQL.Add(':quantidade,');
  qry.SQL.Add(':valor_unitario,');        
  qry.SQL.Add(':perc_desconto');        
  qry.SQL.Add(')');      
    
  qry.ParamByName('id_tbproduto').AsInteger := venda.produto.id;
  qry.ParamByName('data_hora').AsDateTime := venda.dataHora;
  qry.ParamByName('quantidade').AsInteger := venda.quantidade;
  qry.ParamByName('valor_unitario').AsFloat := venda.valorUnitario; 
  qry.ParamByName('perc_desconto').AsFloat := venda.PercentualDesconto;

  qry.ExecSQL;
end;

function TVenda.getDataHora: TdateTime;
begin
  Result := FDataHora;
end;

function TVenda.getId: Integer;
begin
  Result := FId;
end;

function TVenda.getPercentualDesconto: Double;
begin
  Result:= PercentualDesconto;
end;

function TVenda.getProduto: TProduto;
begin
  Result := produto;
end;

function TVenda.getQuantidade: Integer;
begin
  Result := FQuantidade;
end;

function TVenda.getValorUnitario: Double;
begin
  Result:= FValorUnitario;
end;

procedure TVenda.setDataHora(const Value: TdateTime);
begin
  FDataHora := Value;
end;

procedure TVenda.setId(const Value: Integer);
begin
  FId := Value;
end;

procedure TVenda.setPercentualDesconto(const Value: Double);
begin
  FPercentualDesconto := Value;
end;

procedure TVenda.setProduto(const Value: TProduto);
begin
  FProduto := Value;
end;

procedure TVenda.setQuantidade(const Value: Integer);
begin
  FQuantidade := Value;
end;

procedure TVenda.setValorUnitario(const Value: Double);
begin
  FValorUnitario := Value;
end;

end.
