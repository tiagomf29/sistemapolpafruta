unit UEstoque;

interface

uses
  UProduto, ZDataSet, UDM;

type

  TEstoque = class

  private
    FProduto: TProduto;
    FQuantidade: Integer;
    function getProduto: TProduto;
    function getQuantidade: Integer;
    procedure setProduto(const Value: TProduto);
    procedure setQuantidade(const Value: Integer);

  public
    property produto: TProduto read getProduto write setProduto;
    property quantidade: Integer read getQuantidade write setQuantidade;
    procedure insereAtualizaProdutoEstoque(estoque : TEstoque);

  end;

implementation

{ TEstoque }

{ TEstoque }

function TEstoque.getProduto: TProduto;
begin
  Result := FProduto;
end;

function TEstoque.getQuantidade: Integer;
begin
  Result := FQuantidade;
end;

procedure TEstoque.insereAtualizaProdutoEstoque(estoque: TEstoque);
var 
  qry : TZQuery;
begin

  qry := TZQuery.Create(nil);
  qry.Connection:= Dm.conexao;  

  qry.Close;
  qry.SQL.Clear;
  qry.SQL.Add('insert into estoque (id_tbproduto, qtde)');
  qry.SQL.Add('values');
  qry.SQL.Add('(');
  qry.SQL.Add(':id_tbproduto, :qtde');
  qry.SQL.Add(')'); 
  qry.SQL.Add('on duplicate key update qtde = (qtde + :qtde)');
  
  qry.ParamByName('id_tbproduto').AsInteger := estoque.produto.id;
  qry.ParamByName('qtde').AsInteger := estoque.quantidade;

  qry.ExecSQL;
  
end;

procedure TEstoque.setProduto(const Value: TProduto);
begin
  FProduto := Value;
end;

procedure TEstoque.setQuantidade(const Value: Integer);
begin
  FQuantidade := Value;
end;

end.
